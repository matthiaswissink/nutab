document.addEventListener('DOMContentLoaded', function () {

    setStoredBackground();

    onStartUp();

    $('#open-menu-btn, #close-menu-btn, #open-menu-overlay').click(function () {
        var sideNav = $('#side-nav');
        var overlay = $('#open-menu-overlay');
        if (sideNav.hasClass('open')) {
            sideNav.removeClass('open');
            overlay.removeClass('in');
        } else {
            sideNav.addClass('open');
            overlay.addClass('in');
        }
    });


    document.onkeydown = function (e) {
        if (e.keyCode === 39) {
            sliderRight();
        } else if (e.keyCode === 37) {
            sliderLeft();
        }
    };


    $('#arrow-right').click(function () {
        sliderRight();
    });
    $('#arrow-left').click(function () {
        sliderLeft();
    });


    $("input:radio[name=subject]").on('change', function () {
        var subject = $(this).prop('id');
        saveData('subject', subject);
        updateNuContent(subject);
    });

    $("input:radio[name=background-type]").on('change', function () {
        var backgroundType = $(this).prop('id');
        saveData('background-type', backgroundType);
        updateBackground(backgroundType);
    });

});

function onStartUp() {
    getStoredData('subject', function (response) {
        if (response != '') {
            $('input[id=' + response).prop('checked', true);
        } else {
            $('input[id=algemeen]').prop('checked', true);
            response = "algemeen";
        }
        updateNuContent(response);
    });

    getStoredData('background-type', function (response) {
        if (response != '') {
            $('input[id=' + response).prop('checked', true);
        } else {
            $('input[id=nu-in-beeld]').prop('checked', true);
            response = "nu-in-beeld";
        }
        updateBackground(response);
    });
}


function sliderRight() {
    $('.news-wrapper .mdl-card').removeClass('shown right').addClass('left');
    $('.news-wrapper').append($('.news-wrapper').children()[2]);
}
function sliderLeft() {
    $('.news-wrapper .mdl-card').removeClass('shown left').addClass('right');
    $('.news-wrapper').children().last().insertAfter($('.news-wrapper').children()[1]);
}



function setStoredBackground() {
    getStoredData('img_content', function (response) {
        $('#background-copyright').text(response);
    });
    getStoredData('img_url', function (response) {
        $('.background-img').css('background', '#eee url(' + response + ') center / cover');
    });
}



function updateBackground(type) {
    var nuManager = new NuManager();

    nuManager.getBackground(type, function (response) {
        if (typeof response != 'undefined' && response) {
            $('.background-img').css('background', '#eee url(' + response.url + ') center / cover');
            $('#background-copyright').text(response.caption);

            saveData('img_url', response.url);
            saveData('img_content', response.caption);

        } else {
            setStoredBackground();
        }
    });
}



function updateNuContent(subject) {
    var nuManager = new NuManager();

    $('.news-wrapper .mdl-card').remove();
    nuManager.getNuContent(subject, function (response) {
        var channel = response.rss.channel;
        var itemLength = channel.item.length;
        for (var i = 0; i < itemLength; i++) {
            $('.news-wrapper').append(createCard(channel.item[i]));
        }
    }, function (error) {
        console.log(error);
    });
}



function createCard(item) {

    var card = new Card();

    card.setImage(item.enclosure['@attributes'].url);
    card.setText(item.title['#text'], item.description['#text']);
    card.setLink(item.link['#text']);

    return card.getCard();

}


