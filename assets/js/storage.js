function saveData(key, val) {
    var obj = {};
    obj[key] = val;
    chrome.storage.sync.set(obj);
}

function getStoredData(key, callback) {
    var obj = {};
    obj[key] = '';
    chrome.storage.sync.get(obj, function (item) {
        var arr = Object.keys(item).map(function (key) {
            return item[key];
        });
        callback(arr[0]);
    });
}