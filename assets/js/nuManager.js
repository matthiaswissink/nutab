var NuManager = function () {

    this.baseUrl = "http://www.nu.nl/";

};

NuManager.prototype.getBackground = function(type, callback) {

    $.get(this.baseUrl, function (data, status) {
        if (status == "success") {
            var html = $(data);
            var url = html.find("div[data-slug=" + type + "] a").attr("href").replace('640', '1920');
            getStoredData('img_url', function (response) {
                if (response == url)
                    return false;
            });

            var caption = html.find("div[data-slug=" + type + "] a").find(".rsCaption").first().text();
            var response = {"url": url, "caption": caption};
            callback(response);
        }
    });

};

NuManager.prototype.getNuContent = function (subject, callback, errorCallback) {

    var url = this.baseUrl + 'rss/' + subject;
    var x = new XMLHttpRequest();
    x.open('GET', url);
    x.onload = function () {
        var response = xmlToJson(x.responseXML);
        callback(response);
    };
    x.onerror = function () {
        errorCallback('Netwerk error');
    };
    x.send();

};
//
//     getBackground: function (type, callback) {
//         $.get(s.baseUrl, function (data, status) {
//             if (status == "success") {
//                 var html = $(data);
//                 var url = html.find("div[data-slug=" + type + "] a").attr("href").replace('640', '1920');
//                 getStoredData('img_url', function (response) {
//                     if (response == url)
//                         return false;
//                 });
//
//                 var caption = html.find("div[data-slug=" + type + "] a").find(".rsCaption").first().text();
//                 var response = {"url": url, "caption": caption};
//                 callback(response);
//             }
//         });
//     },
//
//     getNuContent: function (subject, callback, errorCallback) {
//         var url = s.baseUrl + 'rss/' + subject;
//         var x = new XMLHttpRequest();
//         x.open('GET', url);
//         x.onload = function () {
//             var response = xmlToJson(x.responseXML);
//             callback(response);
//         };
//         x.onerror = function () {
//             errorCallback('Netwerk error');
//         };
//         x.send();
//     }
//
// };

// function getBackground(type, callback) {
//
//     $.get("http://www.nu.nl", function (data, status) {
//         if (status == "success") {
//             var html = $(data);
//             var url = html.find("div[data-slug=" + type + "] a").attr("href").replace('640', '1920');
//             getStoredData('img_url', function (response) {
//                 if (response == url)
//                     return false;
//             });
//
//             var caption = html.find("div[data-slug=" + type + "] a").find(".rsCaption").first().text();
//             var response = {"url": url, "caption": caption};
//             callback(response);
//         }
//     });
//
// }


// function getNuContent(subject, callback, errorCallback) {
//     var url = 'http://www.nu.nl/rss/' + subject;
//     var x = new XMLHttpRequest();
//     x.open('GET', url);
//     x.onload = function () {
//         var response = xmlToJson(x.responseXML);
//         callback(response);
//     };
//     x.onerror = function () {
//         errorCallback('Netwerk error');
//     };
//     x.send();
// }