var Card = function () {
    this.card = $('<div class="mdl-card mdl-shadow--2dp">' +
        '<div id="card-image" class="mdl-card__title" style="background: url() center / cover;"></div>' +
        '<div id="card-text" class="mdl-card__supporting-text"></div>' +
        '<div class="mdl-card__actions mdl-card--border">' +
        '<a id="card-link" href="" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">Naar artikel</a>' +
        '</div></div>');

};

Card.prototype.setImage = function (image) {
    this.card.find("#card-image").css({"background": "url(" + image + ") center / cover"})
};

Card.prototype.setText = function (title, text) {
    this.card.find("#card-text").html('<strong>' + title + '</strong><br><br>' + text)
};

Card.prototype.setLink = function (link) {
    this.card.find("#card-link").prop("href", link)
};

Card.prototype.getCard = function () {
    return this.card;
};